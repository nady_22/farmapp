/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applefarm;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Nadeem Class to test the methods that has side effects
 */
public class AppleFarmTest {

    public AppleFarmTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        System.out.println("* AppleFarmTest: @BeforeClass method");
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        System.out.println("* AppleFarmTest: @AfterClass method");
    }

    @Before
    public void setUp() {
        System.out.println("* AppleFarmTest: @Before method");
    }

    @After
    public void tearDown() {
        System.out.println("* AppleFarmTest: @After method");
    }

    /**
     * Test of main method, of class AppleFarm.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        AppleFarm.main(args);
    }

    /**
     * Test of getInput method, of class AppleFarm.
     */
    @Test
    public void testGetInput() {
        System.out.println("getInput");
        AppleFarm inputOutput = new AppleFarm();
        String expResult = "add 5";
        InputStream in = new ByteArrayInputStream(expResult.getBytes());
        System.setIn(in);
        String result = inputOutput.getInput();
        assertEquals(expResult, result);
    }

    /**
     * Test of visitFarm method, of class AppleFarm.
     */
    @Test
    @Ignore
    public void testVisitFarm() {
        System.out.println("visitFarm");
        int applesInFarm[][] = new int[3][3];
        int rNumber = 3;
        int cNumber = 3;
        Scanner scan = new Scanner(System.in);
        String simulatedUserInput = "1" + System.getProperty("line.separator");
        //"4" + System.getProperty("line.separator") +  "4" + System.getProperty("line.separator") +  "4" + System.getProperty("line.separator")+
        //"0" + System.getProperty("line.separator") +  "0" + System.getProperty("line.separator") +  "1" + System.getProperty("line.separator")+
        //"2" + System.getProperty("line.separator") +  "1" + System.getProperty("line.separator") +  "3" + System.getProperty("line.separator");
        InputStream savedStandardInputStream = System.in;
        //try{
        System.setIn(new ByteArrayInputStream(simulatedUserInput.getBytes()));
        // code that needs multiple user inputs
        AppleFarm.AssignFarmBoundary(scan, rNumber, cNumber);
        System.setIn(savedStandardInputStream);
    }

    /**
     * Test of AssignFarmBoundary method, of class AppleFarm.
     */
    @Test
    @Ignore
    public void testAssignFarmBoundary() {
        int rNumber = 3;
        int cNumber = 3;
        Scanner scan = new Scanner(System.in);
        System.out.println("AssignFarmBoundary");
        AppleFarm.AssignFarmBoundary(scan, rNumber, cNumber);
    }

    /**
     * Test of collectApples method, of class AppleFarm. Final value expResult
     * is after adding 2 tokens-double the fruits logic
     */
    @Test(timeout = 1000)
    public void testCollectApples() {
        System.out.println("collectApples");
        int[][] applesinFarm = {{4, 0, 1}, {1, 0, 0}, {0, 4, 0}};
        int expResult = 30;
        int result = AppleFarm.collectApples(applesinFarm);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of DisplayFarmMap method, of class AppleFarm.
     */
    @Test
    public void testDisplayFarmMap() {
        System.out.println("DisplayFarmMap");
        int[][] applesPrintArray = {{4, 0, 1}, {1, 0, 0}, {0, 4, 0}};
        AppleFarm.DisplayFarmMap(applesPrintArray);
        // TODO review the generated test code and remove the default call to fail.
    }

}
