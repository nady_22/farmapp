/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applefarm;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Nadeem Given a 2D array with a weight find the max length path. Add
 * two tokens optionally to the largest 2 values and use one token at the given
 * square
 */
public class AppleFarm {

    private static PrintStream output = System.out;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        visitFarm(new Scanner(System.in));
    }//main ends

    /* 
    Method to test the Junit Library.
     */
    public String getInput() {
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    /* 
    This method checks for input conditions and runs a farm menu (operations)
     */
    public static void visitFarm(Scanner scan) {
        output.println(" Press 1: If you would like to create a new 2D array \n Press 2: If you wish to use the default assignment e.g. array: \n Press 0: To exit ? \n ");
        int range, smallest = 0, input;
        for (;;) {
            boolean error = false;
            scan = new Scanner(System.in);
            output.println("Enter your option :  ");
            if (!scan.hasNextInt()) {
                System.out.println("Invalid input! \n");
                continue;
            }
            range = scan.nextInt();
            if (range > 2 || range < 0) {
                System.out.println("Invalid input!");
                error = true;
            }
            if (error) {
                System.out.print("You pressed wrong option :  \n ");
            } else {
                break;
            }
        }
        switch (range) {
            case 0:
                output.println("Program Exists...");
                System.exit(0);
                break;
            case 1:
                int counter = 1;
                int fRows = 0;
                int fColumns = 0;
                output.println("Enter the number of rows in 2D-Array : ");
                while (true) {
                    if (scan.hasNextInt()) {
                        int newInt = scan.nextInt();
                        if (counter == 1) {
                            fRows = newInt;
                            counter++;
                            output.println("Enter the number of cols in 2D-Array : ");
                            continue;
                        } else if (counter == 2) {
                            fColumns = newInt;
                            break;
                        }
                    }
                    output.println("Invalid input!");
                    scan.next();
                }
                AssignFarmBoundary(scan, fRows, fColumns);
                break;
            case 2:
                int[][] array = {{4, 0, 1}, {1, 0, 0}, {0, 4, 0}};
                //Now lets calculate the max no of apples we can collect
                int total = collectApples(array);
                output.println("The Max no of  apples that can be collected are : " + total + "\n");
                break;

        }
    }

    /* This method assigns No of apples to a two dimensional array
       @param args rowNumber and ColumnNumber
     */
    public static void AssignFarmBoundary(Scanner scan, int rNumber, int cNumber) {
        int[][] applesInFarm = new int[rNumber][cNumber];
        int rowCount = 1;
        for (int index = 0; index < rNumber; index++) {
            output.println("Please type in no of apples in row " + rowCount + "\n ");
            for (int indexOfExam = 0; indexOfExam < cNumber; indexOfExam++) {
                applesInFarm[index][indexOfExam] = scan.nextInt();
            }
            rowCount++;
        }
        //Now let's print a two dimensional array in Java
        DisplayFarmMap(applesInFarm);
        //Now lets calculate the max no of apples we can collect
        int totalApples = collectApples(applesInFarm);
        output.println("The Max no of  apples that can be collected are : " + totalApples);
    }//AssignFarmBoundary() ends

    /*  Description: Method to sum the no of apples based on left-bottom corner to top-right corner in 2D Array.
        @param 2D Integer array filled with input values.
        Algorithm :  Collecting Apples by using iterative approach / dynamic programming
        init: matrix[1..n(rows),1....m(columns)]
        Startpoint of Array: Set to rows=n-1,cols=0
            Programs Start at (rows,cols): Does nested for loops for rows and cols with the startpoint.
            STEP 1) Base case : if we at startpoint, the value equals m(rows,cols) 
            STEP 2) We can't come from the bottom,  examine if we're in the bottom row, if(rows == r-1) than m(rows,cols) value is maxValues[i][j-1]) +  m(rows,cols)
            STEP 3) We can't come from the left, if we're in the left corner, if(cols == 0 )than m(rows,cols) value is maxValues[i+1][j]) +  m(rows,cols)
            STEP 4) Final condition, choose between top and right take whichever gives you higher value and return..
            STEP 5) Using tournament algorithm approach we also check which are the largest two numbers in the 2D array and use their location to double the m(rows,cols) value 
     */
    public static int collectApples(int[][] applesinFarm) {
        int result = 0;
        long startTime = 0;
        long endTime = 0;
        int r = applesinFarm.length;
        int c = applesinFarm[0].length;
        output.println("The row length is " + r + "column length is " + c);
        int[][] maxValues = new int[r][c];
        //output.println("The initial max value arrey has " + Arrays.deepToString(maxValues));
        startTime = System.nanoTime(); //time the algorithm started.
        int max1 = 1;
        int max2 = applesinFarm[r - 1][0];
        int max1row = 0;
        int max2row = r - 1;
        int max1col = 0;
        int max2col = 0;
        for (int rows = r - 1; rows >= 0; rows--) {
            for (int cols = 0; cols < c; cols++) {
                //check the largest two numbers available in the 2D array.
                if (applesinFarm[rows][cols] > max1) {
                    max2 = max1;
                    max2row = max1row;
                    max2col = max1col;
                    max1row = rows;
                    max1col = cols;
                    max1 = applesinFarm[rows][cols];
                } else if (applesinFarm[rows][cols] > max2) {
                    max2 = applesinFarm[rows][cols];
                    max2row = rows;
                    max2col = cols;
                }
                //System.out.println("max1 is " + max1 + "max2 is " + max2 + "max1 row is "+ max1row + "max1 col is "+  max1col  + "max2 row is "+  max2row + "max2 col is "+ max2col + "\n");
                if (rows == r - 1 && cols == 0) {
                    maxValues[rows][cols] = applesinFarm[rows][cols];
                } else if (rows == r - 1) {
                    maxValues[rows][cols] = maxValues[rows][cols - 1] + applesinFarm[rows][cols];
                } else if (cols == 0) {
                    maxValues[rows][cols] = maxValues[rows + 1][cols] + applesinFarm[rows][cols];
                } else {
                    maxValues[rows][cols] = Math.max(maxValues[rows][cols - 1], maxValues[rows + 1][cols]) + applesinFarm[rows][cols];
                }
            }
        }
        endTime = System.nanoTime(); //time the algorithm ended.
        output.println("That took " + (endTime - startTime) + " milliseconds");
        output.println("Now the farm has " + Arrays.deepToString(maxValues));
        //use the 2 tokens to double the value once on 1st and 2nd largest tree 
        int doubleApples = doubleApplesinFarm(applesinFarm[max1row][max1col], applesinFarm[max2row][max2col]);
        result = maxValues[0][c - 1] + doubleApples; //result now present in the top-right corner
        output.println("The Apples will now be increased by value = " + doubleApples + " after using the token1 = " + applesinFarm[max1row][max1col] + " and token2 = " + applesinFarm[max2row][max2col]);
        return result;
    }//collectApples() ends

    /*
    Method to double the array based on 2 largest square values in the 2Darray.
    Description: Accepts two largest m and n in 2D array, doubles the value and subtracts already added value in parent function.
    @params : Two largest values m and n as tokens
    @returns: Sum of two tokens
     */
    public static int doubleApplesinFarm(int m, int n) {
        int totalValue;
        int token1 = (m * m) - m; //value of m is included in the iterative approach
        int token2 = (n * n) - n; //value of m is included in the iterative approach 
        //output.println("After doubling the apples m = " + m + " value = " + token1 + " and n = " + n + " value = " + token2);
        totalValue = token1 + token1;
        return totalValue;
    }//doubleApplesinFarm() ends

    /* Method to print/display a two dimensional array in Java */
    public static void DisplayFarmMap(int[][] applesPrintArray) {
        output.println("2D-Array: Apples In The Farm Appear like \n");
        for (int[] a : applesPrintArray) {
            for (int i : a) {
                output.print(i + "\t");
            }
            output.println("\n");
        }
        output.println(Arrays.deepToString(applesPrintArray));
    }//DisplayFarmMap() ends
}
