"#My assignment READ ME"
### Given a 2D array find the max value in a length path. ###

**Description of **: Method to sum the no of apples based on left-bottom corner to top-right corner in 2D Array.

**Parameters** 2D Integer array filled with input values.

**Algorithm** :  Collecting Apples by using iterative / dynamic programming.

**init**: matrix[1..n(rows),1....m(columns)]

**Startpoint of Array**: Set to rows=n-1,cols=0

# How the algorithm works when you run it ?  #

1. Run: Programs Start at (rows,cols): Does nested for loops for rows and cols with the startpoint.

2. STEP 1) Base case : if we at startpoint, the value equals m(rows,cols)

3. STEP 2) We can't come from the bottom if we're in the bottom row, if(rows == r-1) than m(rows,cols) value is index(rows][cols-1]) +  m(rows,cols)

4. STEP 3) We can't come from the left, if we're in the left corner, if(cols == 0 )than m(rows,cols) value is index(rows][cols-1]) +  m(rows,cols)

5. STEP 4) Final condition, choose between top and right take whichever gives you higher value and return..

6. STEP 5) Using tournament algorithm approach: Adds tokens optionally to each of the largest 2 values in a square and use one token each at the given square

# How to run the program.  #
1. Clone the Project from BitBucket and Import the source code and RUN the project in your favorite IDE.
2. Download the AppleFarm.jar file and CD to the location of the source-file and run Java -jar AppleFarm.jar

# What do you see when you run the program. #

**Case 1a: If you choose the Menu option 1 and add in the values correctly as shown below:
**
![correcnt_entry_success.jpg](https://bitbucket.org/repo/8xz4qj/images/7240627-correcnt_entry_success.jpg)

**Case 1b: Use the option of two tokens optionally to the largest 2 values and use one token each at the given square. This time we create array with repeated numbers which changes the max value & square path as shown below:
**
![result_explained_with_print.jpg](https://bitbucket.org/repo/8xz4qj/images/1462565602-result_explained_with_print.jpg)
**Case 2: If you choose the Menu option 2 and use the example array values as shown below :
**
![option_2_assignment_example_success.jpg](https://bitbucket.org/repo/8xz4qj/images/2712468101-option_2_assignment_example_success.jpg)

**Case 3a: Alternatively you can choose the validation of the program as shown below :
**
![condition_1_validation.jpg](https://bitbucket.org/repo/8xz4qj/images/3061635782-condition_1_validation.jpg)

**Case 3b: Alternatively you can choose the validation of the program as shown below :
**
![condition_2_validation.jpg](https://bitbucket.org/repo/8xz4qj/images/4224664861-condition_2_validation.jpg)